# Tools I've used

* [skilstak.io](https://skilstak.io) - where I learn stuff
* [Netlify](https://netlify.com) -  where I host stuff
* [GitLab](https://gitlab.com) - where project source is kept 
* [Vistual Studio Code](https://code.visualstudio.com/download) - projects created with
* [PiskelApp](https://psikelapp.com) - All drawings in the site for the game were made here
* [gfycat.com](https://gfycat.com/gifs/search/graphics+background+video) - Where I got the great background