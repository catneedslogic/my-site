const KEYS = 'LEFT,RIGHT,UP,DOWN,SPACE,W,A,S,D,TAB'

let = p1

class Main extends Phaser.Scene {
preload() {
    this.load.image('p1', 'assets/img/blue_tank.png')
    this.load.image('p2', 'assets/img/red_tank.png')
    this.load.image('bg', 'assets/img/background.png')

    this.load.audio('lasersnd', 'assets/snd/laser.mp3')
}

create() {
    this.add.image(0, 0, 'bg').setOrigin(0, 0)

    p1 = this.physics.add.sprite(30,30, 'p1')

}

update() {}
}

let game = new Phaser.Game({
    scene: Main,
    physics: 'arcade',
    pixelArt: true,
})