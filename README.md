<video autoplay muted loop id="myVideo"><source src="/assets/space_background.mp4" type="video/mp4"></video>
<div class="content">

# Catneedslogic's Site{.maintitle}

<marquee direction="down" width="350px" height="200px" behavior="alternate"><marquee behavior="alternate">A site for fun games</marquee></marquee>

[Check out me game:]{.fmttext}

<div id=gcblock>

<div class=gblock>

[My Eightball Game](/eightball/){.cyan}

[![](assets/ezgif.com-gif-maker.gif)](/eightball/){.pimg}

</div>

<div class=gblock>

[My Phaser Game](/platformer/){.green}

[![](assets/ezgif.com-crop.gif)](/platformer/){.pimg} 

</div>

</div>

<marquee><q>You found a secret</q></marquee>