const keys = 'LEFT,RIGHT,UP,DOWN,SPACE,W,A,S,D,R'
let DD = 2 // deadly delay 
let pl, k, plats, coins, bad, jump, onplat, spawnTimer, score, hscore, spawnCC, music, onFloor
let go = false
let gw = false

// const cubed = some => some ** 3
// const double = wait => wait * 2

const randint = limit => Math.floor(Math.random() * limit)
const randsign = () => Math.random() > .5 ? 1 : -1
const randx = () => randint(game.config.width)
const randy = () => randint(game.config.height)
const rr = (min, max) => randint(max - min + 1) + min

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

let badguys = [
  'bad',
  'bad2',
  'bad3',
  'bad4',
  'bad5',
]

// const randsign = () => {
//   let nr = Math.random()
//   if (nr > .5) {
//     i = 1
//   }
//   if (nr < .5) {
//     i = -1
//   }
//   return i
// }

// const randint = (limit) => {
//   let r = Math.random()
//   r = r * limit
//   r = Math.floor(r)
//   return r
// }

// randx = () => {
//   let x = randint(game.config.width)
//   return x
// }

// randy = () => {
//   let y = randint(game.config.width)
//   return y
// }

// const randrange = (min, max) => {
// let r = max - min + 1
// let y = randint(r) + min
// return y
// }

class Main extends Phaser.Scene {

  init() {
    let hs = localStorage.getItem("hscore")
    if (hs) hscore = hs
    if (!hscore) hscore = 0
    if (!score) score = 0
  }

  preload() {
    this.load.image('player', './assets/img/mysprite.png')
    this.load.image('bg', './assets/img/justbg.png')
    this.load.image('plat', './assets/img/Platform(1).png')
    this.load.image('bad', './assets/img/badguy.png')
    this.load.image('bad2', './assets/img/badguyv2.png')
    this.load.image('bad3', './assets/img/badguyv3.png')
    this.load.image('bad4', './assets/img/badguyv4.png')
    this.load.image('bad5', './assets/img/badguyv5.png')
    this.load.image('coin', './assets/img/coin.png')
    this.load.image('floor', './assets/img/onlyonetoperfection.png')
    this.load.image('go', './assets/img/gameover.png')
    this.load.image('gw', './assets/img/youwin.png')

    this.load.audio('hs', './assets/snd/hit.wav')
    this.load.audio('bgsound', './assets/snd/background_music.wav')
    this.load.audio('scoin', './assets/snd/coin_sound.wav')
    this.load.audio('jumps', './assets/snd/jump.wav')
    this.load.audio('gos', './assets/snd/gameover.wav')
    this.load.audio('gws', './assets/snd/win_sound.wav')
  }

  create() {
    this.add.image(0, 0, 'bg').setOrigin(0, 0)

    pl = this.physics.add.sprite(10, 100, 'player')
    pl.setCollideWorldBounds(true)
    pl.setGravityY(1200)


    //bad = this.physics.add.sprite(100, 100, 'bad')
    bad = this.physics.add.group()

    const spawnEnemy = () => {
      let b = bad.create(randx(), randy(), badguys[randint(5)])
      b.setCollideWorldBounds(true)
      b.setVelocityY(230 * randsign())
      b.setVelocityX(230 * randsign())
      b.setBounce(1)
      b.setScale(.3)
      sleep(500).then(() => {
        b.setScale(.5)
      })
      sleep(1000).then(() => {
        b.setScale(.7)
      })
      b.deadly = false
      setTimeout(() => {
        b.setScale(1)
        b.deadly = true
      }, DD * 1500)
    }

    spawnEnemy()

    spawnTimer = setInterval(spawnEnemy, 1000)

    // If you want to set a floor .setScale(16, 1)

    plats = this.physics.add.staticGroup()
    plats.create(200, 300, 'plat')
    plats.create(400, 300, 'plat')
    plats.create(600, 200, 'plat')
    plats.create(800, 200, 'plat')
    plats.create(400, 200, 'plat')
    plats.create(130, 160, 'plat')
    plats.create(65, 500, 'plat')
    plats.create(200, 600, 'plat')
    plats.create(800, 600, 'plat')
    plats.create(900, 500, 'plat')
    plats.create(500, 580, 'plat')
    plats.create(512, 700, 'floor')
    plats.create(0, 380, 'floor')
      .setOrigin(-.07, 0)
      .refreshBody()

    coins = this.physics.add.group()

    const spawnCoin = (x, y) => {
      coins.create(x || randx(), y || randy(), 'coin')
    }

    // spawnCC = setInterval(spawnCoin, 1000)
    for (let n = 0; n < 10; n++) spawnCoin()

    score = 0
    let scoreText = this.add.text(16, 16, `Score: ${score}`, {
      fontFamily: "ariel",
      color: "green",
      fontSize: "32px",
    })

    let hscoreText = this.add.text(800, 16, `High score: ${hscore}`, {
      fontFamily: "ariel",
      color: "green",
      fontSize: "32px",
      align: "right",
    })

    let pickup = this.sound.add('scoin')
    music = this.sound.add('bgsound')
    jump = this.sound.add('jumps')
    let hits = this.sound.add('hs')
    let gameo = this.sound.add('gos')
    let gamew = this.sound.add('gws')
    music.play({
      volume: .8,
      loop: true,
      seek: 4,
      // rate: 1, this is to slow it down.Goes 1-.5 - 0
    })

    let collideCoin = (p, c) => {
      c.destroy()
      spawnCoin()
      score += 1  // same as score = score + 1
      scoreText.setText(`Score: ${score}`)
      pickup.play()
      if (score == 20) {
        clearInterval(spawnTimer)
        clearInterval(spawnCC)
        this.physics.pause()
        music.stop()
        gw = true
        gamew.play()
        this.add.image(0, 0, 'gw').setOrigin(0, 0).setScale(1.1)
      }
      if (score > hscore) {
        hscore = score
        hscoreText.setText(`High: ${hscore}`)
        localStorage.setItem('hscore', hscore)
      }
    }

    let colliedBad = (p, b) => {
      if (!b.deadly) return
      clearInterval(spawnTimer)
      clearInterval(spawnCC)
      this.physics.pause()
      music.stop()
      go = true
      gameo.play()
      this.add.image(0, 0, 'go').setOrigin(0, 0)
    }

    const collidePlat = (p, pl) => {
      if (!onplat) {
        hits.play()
        onplat = true
      }
      onplat = true
    }

    this.physics.add.collider(pl, plats, collidePlat)
    this.physics.add.collider(pl, plats)
    this.physics.add.collider(pl, bad, colliedBad)
    this.physics.add.collider(pl, bad)
    this.physics.add.collider(plats, bad)
    this.physics.add.collider(pl, coins, collideCoin)

    k = this.input.keyboard.addKeys(keys)

    // this.cameras.main.setBounds(0,0,1024,768)
    // this.cameras.main.startFollow(pl,true)
    // this.cameras.main.setZoom(2)

  }

  update() {

    // if (pl.body.)

    if (k.R.isDown) {
      this.scene.restart()
      clearInterval(spawnTimer)
      music.stop()
    }

    if (k.LEFT.isDown || k.A.isDown) {
      pl.setVelocityX(-400)
    }

    else if (k.RIGHT.isDown || k.D.isDown) {
      pl.setVelocityX(400)
    }

    if (pl.body.onFloor()) {
      pl.setDragX(6000)
      if (k.UP.isDown || k.SPACE.isDown || k.W.isDown) {
        pl.setVelocityY(-600)
        jump.play({
          volume: .2
        })
      }
    } else {
      pl.setDragX(0)
    }

  }

}

let game = new Phaser.Game({
  scene: Main,
  physics: { default: 'arcade'},
  pixelArt: true,
})