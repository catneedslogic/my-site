const KEYS = 'LEFT,RIGHT,UP,DOWN,SPACE,W,A,S,D,TAB'

let p1, p2, k, fire1, fire2, bullet, lasersnd, anim1, sprite, line1, line2

let defspeed = 3

const newxy = (d, rot) => {
    let x = d * Math.cos(rot)
    let y = d * Math.sin(rot)
    // console.log(x,y)
    return [x, y]
}

const sleep = milliseconds => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

class Main extends Phaser.Scene {
    preload() {
        this.load.image('p1', 'assets/img/blue_tank.png')
        this.load.image('p2', 'assets/img/red_tank.png')
        this.load.image('bg', 'assets/img/background.png')

        this.load.audio('lasersnd', 'assets/snd/laser.mp3')
    }
    create() {
        this.add.image(0, 0, 'bg').setOrigin(0, 0)

        p1 = this.add.image(30, 30, 'p1')
        p1.setScale(3)
        p1.speed = defspeed

        p2 = this.add.image(240, 240, 'p2')
        p2.setScale(3)
        p2.speed = defspeed



        lasersnd = this.sound.add('lasersnd')

        fire1 = () => {
            let [dx, dy] = newxy(1200, p1.rotation)
            let [pdx, pdy] = newxy(50, p1.rotation)
            line1 = new Phaser.Geom.Line(p1.x + pdx, p1.y + pdy, p1.x + dx, p1.y + dy)
            let graphics = this.add.graphics({ lineStyle: { width: 4, color: 0x5EF7DE } })
            graphics.strokeLineShape(line1)
            lasersnd.play({
                seek: .3
            })
            sleep(100).then(() => {
                graphics.clear(line1)
                clearAlpha(line1)
            })
        }
        fire2 = () => {
            let [dx, dy] = newxy(1024, p2.rotation)
            let [pdx, pdy] = newxy(50, p2.rotation)
            line2 = new Phaser.Geom.Line(p2.x + pdx, p2.y + pdy, p2.x + dx, p2.y + dy)
            let graphics = this.add.graphics({ lineStyle: { width: 4, color: 0xff5252 } })
            graphics.strokeLineShape(line2)
            sleep(100).then(() => {
                graphics.clear(line2)
                clearAlpha(line2)
            })
        }

        k = this.input.keyboard.addKeys(KEYS)
    }

    update() {
        if (k.LEFT.isDown) {
            p1.x -= p1.speed
        }
        if (k.RIGHT.isDown) {
            p1.x += p1.speed
        }
        if (k.UP.isDown) {
            p1.y -= p1.speed
        }
        if (k.DOWN.isDown) {
            p1.y += p1.speed
        }
        if (k.SPACE.isDown) {
            fire1()
        }

        if (k.UP.isDown && !k.LEFT.isDown && !k.DOWN.isDown && !k.RIGHT.isDown) { p1.angle = -90 }
        if (!k.UP.isDown && !k.LEFT.isDown && k.DOWN.isDown && !k.RIGHT.isDown) { p1.angle = -270 }
        if (k.UP.isDown && k.LEFT.isDown && !k.DOWN.isDown && !k.RIGHT.isDown) { p1.angle = -135 }
        if (k.UP.isDown && !k.LEFT.isDown && !k.DOWN.isDown && k.RIGHT.isDown) { p1.angle = 315 }
        if (!k.UP.isDown && k.LEFT.isDown && !k.DOWN.isDown && !k.RIGHT.isDown) { p1.angle = -180 }
        if (!k.UP.isDown && !k.LEFT.isDown && !k.DOWN.isDown && k.RIGHT.isDown) { p1.angle = 0 }
        if (!k.UP.isDown && !k.LEFT.isDown && k.DOWN.isDown && k.RIGHT.isDown) { p1.angle = 45 }
        if (!k.UP.isDown && k.LEFT.isDown && k.DOWN.isDown && !k.RIGHT.isDown) { p1.angle = 135 }

        if (k.A.isDown) {
            p2.x -= p2.speed
        }
        if (k.D.isDown) {
            p2.x += p2.speed
        }
        if (k.W.isDown) {
            p2.y -= p2.speed
        }
        if (k.S.isDown) {
            p2.y += p2.speed
        }
        if (k.TAB.isDown) {
            fire2()
        }

        if (k.W.isDown && !k.A.isDown && !k.S.isDown && !k.D.isDown) { p2.angle = -90 }
        if (!k.W.isDown && !k.A.isDown && k.S.isDown && !k.D.isDown) { p2.angle = -270 }
        if (k.W.isDown && k.A.isDown && !k.S.isDown && !k.D.isDown) { p2.angle = -135 }
        if (k.W.isDown && !k.A.isDown && !k.S.isDown && k.D.isDown) { p2.angle = 315 }
        if (!k.W.isDown && k.A.isDown && !k.S.isDown && !k.D.isDown) { p2.angle = -180 }
        if (!k.W.isDown && !k.A.isDown && !k.S.isDown && k.D.isDown) { p2.angle = 0 }
        if (!k.W.isDown && !k.A.isDown && k.S.isDown && k.D.isDown) { p2.angle = 45 }
        if (!k.W.isDown && k.A.isDown && k.S.isDown && !k.D.isDown) { p2.angle = 135 }

        

    }
}

let game = new Phaser.Game({
    scene: Main,
    physics: 'arcade',
    pixelArt: true,
})