

class Main extends Phaser.Scene {
  preload(){
    this.load.image('spark', 'assets/particles/blue.png')
  }

  create(){
    //  First create a particle manager
    //  A single manager can be responsible for multiple emitters
    //  The manager also controls which particle texture is used by _all_ emitter
    let particles = this.add.particles('spark');

    let emitter = particles.createEmitter();

    emitter.setPosition(400, 300);
    emitter.setSpeed(200);
    emitter.setBlendMode(Phaser.BlendModes.ADD);}
  update(){}
}

let game = new Phaser.Game({
  scene: Main,
  pixelArt: true,
})